#include <exception>

#include "com/mapswithme/core/jni_helper.hpp"
#include "com/mapswithme/core/ScopedLocalRef.hpp"
#include "platform/saf.hpp"
#include "base/string_utils.hpp"

extern jclass g_saf;

namespace platform
{

JNIEnv * GetEnv()
{
  JNIEnv * env;
  switch(jni::GetJVM()->GetEnv((void **)&env, JNI_VERSION_1_6))
  {
    case JNI_OK:
      break;
    case JNI_EDETACHED:
      if (jni::GetJVM()->AttachCurrentThread((JNIEnv **)&env, NULL) != 0) {
        LOG(LERROR, ("Can't attach JNIEnv to current thread"));
        MYTHROW(RootException, ("Can't attach JNIEnv to current thread"));
      }
      break;
    default:
      LOG(LERROR, ("Can't get JNIEnv. Is the thread attached to JVM?"));
      MYTHROW(RootException, ("Can't get JNIEnv. Is the thread attached to JVM?"));
      break;
  }
  return env;
}

inline bool ends_with(std::string const & value, std::string const & ending)
{
    if (ending.size() > value.size()) return false;
    return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

void RethrowOnJniException(JNIEnv* env)
{
  if (!env->ExceptionCheck())
    return;
  std::string str = jni::DescribeException();
  MYTHROW(RootException, (str));
}

std::string FilesName(int i)
{
  JNIEnv *env = GetEnv();

  static jmethodID SAF_FilesName = jni::GetStaticMethodID(env, g_saf, "FilesName", "(I)Ljava/lang/String;");
  RethrowOnJniException(env);

  jstring str = (jstring) env->CallStaticObjectMethod(g_saf, SAF_FilesName, i);
  RethrowOnJniException(env);

  return jni::ToNativeString(env, str);
}

int FilesType(const std::string & path)
{
  JNIEnv *env = GetEnv();

  static jmethodID SAF_FilesName = jni::GetStaticMethodID(env, g_saf, "FilesType", "(Ljava/lang/String;)I");
  RethrowOnJniException(env);

  jint i = (jint) env->CallStaticIntMethod(g_saf, SAF_FilesName, jni::ToJavaString(env, path));
  RethrowOnJniException(env);

  return i;
}

uint64_t FilesSize(const std::string & path)
{
  JNIEnv *env = GetEnv();

  static jmethodID SAF_FilesName = jni::GetStaticMethodID(env, g_saf, "FilesSize", "(Ljava/lang/String;)J");
  RethrowOnJniException(env);

  jlong j = (jlong) env->CallStaticLongMethod(g_saf, SAF_FilesName, jni::ToJavaString(env, path));
  RethrowOnJniException(env);
  
  return j;
}

int FilesWrite(const std::string &path, uint64_t off, void const *buf, int pos, int size)
{
  JNIEnv *env = GetEnv();

  static jmethodID SAF_FilesName = jni::GetStaticMethodID(env, g_saf, "FilesWrite", "(Ljava/lang/String;J[BII)I");

  jni::ScopedLocalRef<jbyteArray> arr(env, env->NewByteArray(size));
  env->SetByteArrayRegion(arr.get(), pos, size, (jbyte*) buf);
  RethrowOnJniException(env);

  jint i = (jint) env->CallStaticIntMethod(g_saf, SAF_FilesName, jni::ToJavaString(env, path), off, arr.get(), pos, size);
  RethrowOnJniException(env);

  return i;
}

int FilesRead(const std::string &path, uint64_t off, void const *buf, int pos, int size)
{
  JNIEnv *env = GetEnv();

  static jmethodID SAF_FilesName = jni::GetStaticMethodID(env, g_saf, "FilesRead", "(Ljava/lang/String;J[BII)I");

  jni::ScopedLocalRef<jbyteArray> arr(env, env->NewByteArray(size));

  jint i = (jint) env->CallStaticIntMethod(g_saf, SAF_FilesName, jni::ToJavaString(env, path), off, arr.get(), pos, size);
  RethrowOnJniException(env);
  
  env->GetByteArrayRegion(arr, pos, size, (jbyte*) buf);

  RethrowOnJniException(env);

  return i;
}

bool FilesDelete(const std::string &path)
{
  JNIEnv *env = GetEnv();

  static jmethodID SAF_FilesName = jni::GetStaticMethodID(env, g_saf, "FilesDelete", "(Ljava/lang/String;)Z");

  jboolean b = (jboolean) env->CallStaticBooleanMethod(g_saf, SAF_FilesName, jni::ToJavaString(env, path));
  RethrowOnJniException(env);

  return b;
}

bool FilesMkdir(const std::string &path)
{
  JNIEnv *env = GetEnv();

  static jmethodID SAF_FilesName = jni::GetStaticMethodID(env, g_saf, "FilesMkdir", "(Ljava/lang/String;)Z");
  
  jboolean b = (jboolean) env->CallStaticBooleanMethod(g_saf, SAF_FilesName, jni::ToJavaString(env, path));
  RethrowOnJniException(env);
  
  return b;
}

bool FilesMove(const std::string &from, const std::string &to)
{
  JNIEnv *env = GetEnv();

  static jmethodID SAF_FilesName = jni::GetStaticMethodID(env, g_saf, "FilesMove", "(Ljava/lang/String;Ljava/lang/String;)Z");
  
  jboolean b = (jboolean) env->CallStaticBooleanMethod(g_saf, SAF_FilesName, jni::ToJavaString(env, from), jni::ToJavaString(env, to));
  RethrowOnJniException(env);
  
  return b;
}

int FilesCount(const std::string &path)
{
  JNIEnv *env = GetEnv();

  static jmethodID SAF_FilesCount = jni::GetStaticMethodID(env, g_saf, "FilesCount", "(Ljava/lang/String;)I");

  int i = env->CallStaticIntMethod(g_saf, SAF_FilesCount, jni::ToJavaString(env, path));
  RethrowOnJniException(env);

  return i;
}

void FilesByExt(std::string const& dir, std::string const& ext, Platform::FilesList& files)
{
  int c = FilesCount(dir);
  for(int i = 0; i < c; i++)
  {
    std::string const & name = FilesName(i);
    if (ends_with(name, ext))
      files.push_back(name);
  }
}

bool WriteToTempAndRenameToFile(std::string const & dest, std::function<bool(std::string const &)> const & write)
{
  std::string const tmpFileName = dest + ".tmp" + strings::to_string(std::this_thread::get_id());
  if (!write(tmpFileName))
  {
    LOG(LERROR, ("Can't write to", tmpFileName));
    FilesDelete(tmpFileName);
    return false;
  }
  if (!FilesMove(tmpFileName, dest))
  {
    LOG(LERROR, ("Can't rename file", tmpFileName, "to", dest));
    FilesDelete(tmpFileName);
    return false;
  }
  return true;
}

}

