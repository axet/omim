package com.mapswithme.maps.purchase;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.mapswithme.maps.PurchaseOperationObservable;

import java.util.List;

public class PurchaseFactory
{
  private PurchaseFactory()
  {
    // Utility class.
  }

  public static PurchaseController<PurchaseCallback> createAdsRemovalPurchaseController(
      @NonNull Context context)
  {
    return createSubscriptionPurchaseController(context, SubscriptionType.ADS_REMOVAL);
  }

  public static PurchaseController<PurchaseCallback> createBookmarksAllSubscriptionController(
      @NonNull Context context)
  {
    return createSubscriptionPurchaseController(context, SubscriptionType.BOOKMARKS_ALL);
  }

  public static PurchaseController<PurchaseCallback> createBookmarksSightsSubscriptionController(
      @NonNull Context context)
  {
    return createSubscriptionPurchaseController(context, SubscriptionType.BOOKMARKS_SIGHTS);
  }

  @NonNull
  private static PurchaseController<PurchaseCallback> createSubscriptionPurchaseController(
      @NonNull Context context, @NonNull SubscriptionType type)
  {
    BillingManager<PlayStoreBillingCallback> billingManager
        = new BillingManager<PlayStoreBillingCallback>() { public void initialize(@NonNull Activity context) {};
      public void destroy() {};
      public void launchBillingFlowForProduct(@NonNull String productId) {};
      public boolean isBillingSupported() {  return false; };
      public void queryExistingPurchases() {};
      public void queryProductDetails(@NonNull String... productIds) {}
      public void addCallback(@NonNull PlayStoreBillingCallback callback) {}
      public void queryProductDetails(@NonNull List<String> productIds) {}
      public void consumePurchase(@NonNull String purchaseToken) {}
      public void removeCallback(@NonNull PlayStoreBillingCallback callback) {}
    };
    PurchaseOperationObservable observable = PurchaseOperationObservable.from(context);
    PurchaseValidator<ValidationCallback> validator = new DefaultPurchaseValidator(observable);
    String[] productIds = type.getProductIds();
    return new SubscriptionPurchaseController(validator, billingManager, type, productIds);
  }

  @NonNull
  static PurchaseController<PurchaseCallback> createBookmarkPurchaseController(
      @NonNull Context context, @Nullable String productId, @Nullable String serverId)
  {
    return createAdsRemovalPurchaseController(context);
  }

  @NonNull
  public static PurchaseController<FailedPurchaseChecker> createFailedBookmarkPurchaseController(
      @NonNull Context context)
  {
    PurchaseOperationObservable observable = PurchaseOperationObservable.from(context);
    PurchaseValidator<ValidationCallback> validator = new DefaultPurchaseValidator(observable);
    return new FailedBookmarkPurchaseController(validator, new BillingManager<PlayStoreBillingCallback>() { public void initialize(@NonNull Activity context) {};
      public void destroy() {};
      public void launchBillingFlowForProduct(@NonNull String productId) {};
      public boolean isBillingSupported() {  return false; };
      public void queryExistingPurchases() {};
      public void queryProductDetails(@NonNull String... productIds) {}
      public void addCallback(@NonNull PlayStoreBillingCallback callback) {}
      public void queryProductDetails(@NonNull List<String> productIds) {}
      public void consumePurchase(@NonNull String purchaseToken) {}
      public void removeCallback(@NonNull PlayStoreBillingCallback callback) {} });
  }

  @NonNull
  public static BillingManager<PlayStoreBillingCallback> createInAppBillingManager()
  {
    return new BillingManager<PlayStoreBillingCallback>() {
      public void initialize(@NonNull Activity context) {}
      public void destroy() {}
      public void launchBillingFlowForProduct(@NonNull String productId) {}
      public boolean isBillingSupported() { return false; }
      public void queryExistingPurchases() {}
      public void queryProductDetails(@NonNull List<String> productIds) {}
      public void consumePurchase(@NonNull String purchaseToken) {}
      public void addCallback(@NonNull PlayStoreBillingCallback callback) {}
      public void removeCallback(@NonNull PlayStoreBillingCallback callback) {}
    };
  }

  @NonNull
  static BillingManager<PlayStoreBillingCallback> createSubscriptionBillingManager()
  {
    return new BillingManager<PlayStoreBillingCallback>() { public void initialize(@NonNull Activity context) {};
      public void destroy() {};
      public void launchBillingFlowForProduct(@NonNull String productId) {};
      public boolean isBillingSupported() {  return false; };
      public void queryExistingPurchases() {};
      public void queryProductDetails(@NonNull String... productIds) {}
      public void addCallback(@NonNull PlayStoreBillingCallback callback) {}
      public void queryProductDetails(@NonNull List<String> productIds) {}
      public void consumePurchase(@NonNull String purchaseToken) {}
      public void removeCallback(@NonNull PlayStoreBillingCallback callback) {}
    };
  }
}
