package com.mapswithme.maps.downloader;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.preference.PreferenceManager;

import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.app.Storage;
import com.github.axet.wget.ApacheIndex;
import com.github.axet.wget.RangeSet;
import com.github.axet.wget.WGet;
import com.mapswithme.maps.Framework;
import com.mapswithme.util.NetworkPolicy;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DownloadManager {
    public static final String TAG = DownloadManager.class.getSimpleName();

    public static String LEGACY = "https://gitlab.com/axet/omim/-/raw/master/data/";
    public static SimpleDateFormat DATE = new SimpleDateFormat("yyMMdd", Locale.US);
    public static String PREF_VERSION = "apache_version";
    public static String REMAP = "(.*)\\/(\\d+)\\/(.*)";

    // https://github.com/osmandapp/Osmand/blob/master/OsmAnd-java/src/main/java/net/osmand/util/MapUtils.java#L310
    public static String OSM_URL = "http://osm.org/go/";
    public static final char intToBase64[] = {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '_', '~'
    };

    public static DownloadManager DM = new OrganicsMaps();

    public Handler handler = new Handler(Looper.getMainLooper());
    public Context context;
    public long last; // last update
    public HashMap<String, Master> map = new HashMap<>();
    public Runnable update;
    public Executor executors = Executors.newFixedThreadPool(4);
    public Runnable init = new Runnable() {
        @Override
        public void run() {
            if (context != null) {
                long v = PreferenceManager.getDefaultSharedPreferences(context).getLong(PREF_VERSION, 0);
                if (v > Framework.nativeGetDataVersion())
                    Framework.nativeSetDataVersion(v);
            }
        }
    };
    public Runnable save;

    public static long interleaveBits(long x, long y) {
        long c = 0;
        for (byte b = 31; b >= 0; b--) {
            c = (c << 1) | ((x >> b) & 1);
            c = (c << 1) | ((y >> b) & 1);
        }
        return c;
    }

    public static String createShortLinkString(double latitude, double longitude, int zoom) {
        long lat = (long) (((latitude + 90d) / 180d) * (1L << 32));
        long lon = (long) (((longitude + 180d) / 360d) * (1L << 32));
        long code = interleaveBits(lon, lat);
        String str = "";
        // add eight to the zoom level, which approximates an accuracy of one pixel in a tile.
        for (int i = 0; i < Math.ceil((zoom + 8) / 3d); i++)
            str += intToBase64[(int) ((code >> (58 - 6 * i)) & 0x3f)];
        // append characters onto the end of the string to represent
        // partial zoom levels (characters themselves have a granularity of 3 zoom levels).
        for (int j = 0; j < (zoom + 8) % 3; j++)
            str += '-';
        return str;
    }

    public static class OpenStreetMap extends DownloadManager { // apache index (no /map_version/)
        public String url = "http://download.openstreetmap.ru/mapsme/";
        public ApacheIndex index;

        public String getUrl() {
            return url;
        }

        synchronized public boolean start(ChunkTask k) {
            if (index == null)
                return false;
            String url = filter(k.mUrl);
            url = com.github.axet.androidlibrary.net.HttpClient.safe(url);
            DownloadManager.Master t = map.get(url);
            if (t == null) {
                Log.d(TAG, "legacy url: " + url);
                ApacheIndex.Entry v = index.get(new File(k.mUrl).getName());
                long size = k.mExpectedFileSize;
                if (v != null)
                    size = v.size;
                if (url.startsWith(LEGACY))
                    size = 0;
                t = new DownloadManager.Master(k.mHttpCallbackID, url, k.mBeg, -1, size, null, k.mUserAgent);
                map.put(url, t);
                t.executeOnExecutor(executors, (Void[]) null);
            }
            t.depends(k);
            return true;
        }

        synchronized public void update(boolean force) throws IOException {
            long now = System.currentTimeMillis();
            if (force || index == null || last + 12 * AlarmManager.HOUR1 < now) {
                Log.d(TAG, "Start grabbing: " + url);
                index = new ApacheIndex(url);
                int count = 0;
                long a = 0;
                for (String key : index.keySet()) {
                    ApacheIndex.Entry v = index.get(key);
                    if (v.date != null) {
                        a += v.date.getTime();
                        count++;
                    }
                }
                if (count == 0) {
                    index = null;
                    throw new IOException("Maps index malformed, check your internet connection");
                }
                final long version = Long.valueOf(DATE.format(new Date(a / count)));
                last = now;
                Log.d(TAG, "Grabbed maps version: " + version);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (Framework.nativeGetDataVersion() != version) {
                            Framework.nativeSetDataVersion(version);
                            save = new Runnable() {
                                @Override
                                public void run() {
                                    PreferenceManager.getDefaultSharedPreferences(context).edit().putLong(PREF_VERSION, version).commit();
                                }
                            };
                            if (context != null) {
                                save.run();
                                save = null;
                            }
                        }
                    }
                });
            }
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (context != null && save != null) {
                        save.run();
                        save = null;
                    }
                }
            });
            handler.removeCallbacks(update);
            handler.postDelayed(update, 1 * AlarmManager.HOUR1);
        }
    }

    public static class OriginalMaps extends DownloadManager { // map version and size based on countries.txt
        public String raw = "https://raw.githubusercontent.com/organicmaps/organicmaps/master/data/countries.txt";
        public String url = "https://cdn.organicmaps.app/maps";
        public HashMap<String, JSONObject> countries;

        synchronized public boolean start(ChunkTask k) {
            if (countries == null)
                return false;
            String url = filter(k.mUrl);
            url = com.github.axet.androidlibrary.net.HttpClient.safe(url);
            DownloadManager.Master t = map.get(url);
            if (t == null) {
                Log.d(TAG, "download url: " + url);
                long size = k.mExpectedFileSize;
                String id = Storage.getNameNoExt(Uri.parse(url).getLastPathSegment());
                JSONObject json = null;
                if (countries != null)
                    json = countries.get(id);
                if (json != null) {
                    try {
                        size = json.getLong("s");
                    } catch (JSONException ignore) {
                        size = 0;
                    }
                } else {
                    size = 0;
                }
                t = new DownloadManager.Master(k.mHttpCallbackID, url, k.mBeg, -1, size, null, k.mUserAgent);
                map.put(url, t);
                t.executeOnExecutor(executors, (Void[]) null);
            }
            t.depends(k);
            return true;
        }

        public String getUrl() {
            return url + "/" + Framework.nativeGetDataVersion() + "/";
        }

        public String filter(String url) {
            if (url.isEmpty())
                return url;
            if (url.startsWith("http"))
                return url;
            if (url.endsWith(".ttf") || url.startsWith("World"))
                return getUrl() + url;
            Pattern p = Pattern.compile(REMAP);
            Matcher m = p.matcher(url);
            if (m.matches())
                url = m.group(3);
            return getUrl() + url;
        }

        synchronized public void update(boolean force) throws IOException {
            long now = System.currentTimeMillis();
            if (force || countries == null || last + 12 * AlarmManager.HOUR1 < now) {
                Log.d(TAG, "Start grabbing: " + raw);
                String json = WGet.getHtml(new URL(raw));
                if (json == null || json.isEmpty()) {
                    countries = null;
                    throw new IOException("Maps index malformed, check your internet connection");
                }
                final long version;
                try {
                    JSONObject data = new JSONObject(json);
                    JSONArray gg = data.getJSONArray("g");
                    countries = new HashMap<>();
                    for (int i = 0; i < gg.length(); i++) {
                        JSONObject cc = gg.getJSONObject(i);
                        String id = cc.getString("id");
                        countries.put(id, cc);
                    }
                    version = data.getLong("v");
                    last = now;
                } catch (JSONException e) {
                    throw new IOException(e);
                }
                Log.d(TAG, "Grabbed maps version: " + version);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (Framework.nativeGetDataVersion() != version) {
                            Framework.nativeSetDataVersion(version);
                            save = new Runnable() {
                                @Override
                                public void run() {
                                    PreferenceManager.getDefaultSharedPreferences(context).edit().putLong(PREF_VERSION, version).commit();
                                }
                            };
                            if (context != null) {
                                save.run();
                                save = null;
                            }
                        }
                    }
                });
            }
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (context != null && save != null) {
                        save.run();
                        save = null;
                    }
                }
            });
            handler.removeCallbacks(update);
            handler.postDelayed(update, 1 * AlarmManager.HOUR1);
        }
    }

    public static class OrganicsMaps extends DownloadManager { // map version from original request, no size
        public String raw = "https://raw.githubusercontent.com/organicmaps/organicmaps/master/data/countries.txt";
        public String url = "https://cdn.organicmaps.app/maps";

        synchronized public boolean start(ChunkTask k) {
            if (last == 0)
                return false;
            String url = filter(k.mUrl);
            url = com.github.axet.androidlibrary.net.HttpClient.safe(url);
            DownloadManager.Master t = map.get(url);
            if (t == null) {
                Log.d(TAG, "legacy url: " + url);
                long size = 0; // k.mExpectedFileSize;
                t = new DownloadManager.Master(k.mHttpCallbackID, url, k.mBeg, -1, size, null, k.mUserAgent);
                map.put(url, t);
                t.executeOnExecutor(executors, (Void[]) null);
            }
            t.depends(k);
            return true;
        }

        public String getUrl(String v) {
            return url + "/" + v + "/";
        }

        public String filter(String url) {
            if (url.endsWith(".ttf") || url.startsWith("World"))
                return getUrl("" + Framework.nativeGetDataVersion()) + url;
            Pattern p = Pattern.compile(REMAP);
            Matcher m = p.matcher(url);
            if (!m.matches())
               return url;
            String v = m.group(2);
            return getUrl(v) + m.group(3);
        }

        public String getString(String url, int size) throws IOException {
            HttpURLConnection c = com.github.axet.androidlibrary.net.HttpClient.openConnection(Uri.parse(url));
            InputStream is = c.getInputStream();
            byte[] buf = new byte[size];
            int len = is.read(buf);
            is.close();
            return new String(buf, 0, len, StandardCharsets.UTF_8);
        }

        synchronized public void update(boolean force) throws IOException {
            long now = System.currentTimeMillis();
            if (force || last == 0 || last + 12 * AlarmManager.HOUR1 < now) {
                Log.d(TAG, "Start grabbing: " + raw);
                String json = getString(raw, 1024);
                if (json == null || json.isEmpty()) {
                    last = 0;
                    throw new IOException("Maps index malformed, check your internet connection");
                }
                final long version;
                Pattern p = Pattern.compile("\"v\": (\\d+),");
                Matcher m = p.matcher(json);
                if (m.find())
                    version = Long.parseLong(m.group(1));
                else
                    throw new IOException("unable to read json");
                last = now;
                Log.d(TAG, "Grabbed maps version: " + version);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (Framework.nativeGetDataVersion() != version) {
                            Framework.nativeSetDataVersion(version);
                            save = new Runnable() {
                                @Override
                                public void run() {
                                    PreferenceManager.getDefaultSharedPreferences(context).edit().putLong(PREF_VERSION, version).commit();
                                }
                            };
                            if (context != null) {
                                save.run();
                                save = null;
                            }
                        }
                        cancelDelayed();
                    }
                });
            }
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (context != null && save != null) {
                        save.run();
                        save = null;
                    }
                }
            });
            handler.removeCallbacks(update);
            handler.postDelayed(update, 1 * AlarmManager.HOUR1);
        }
    }

    public class Master extends ChunkTask { // Master (First request) Chunk
        public ArrayList<ChunkTask> deps = new ArrayList<>();
        public int done = 0;
        public RangeSet ranges;

        public Master(long httpCallbackID, String url, long beg, long end, long expectedFileSize, byte[] postBody, String userAgent) {
            super(httpCallbackID, url, beg, end, expectedFileSize, postBody, userAgent);
        }

        @Override
        protected void onProgressUpdate(byte[]... data) {
            super.onProgressUpdate(data);
            purge();
        }

        @Override
        public void onFinish(long id, int err, long beg, long end) {
            done = err;
            for (ChunkTask k : deps)
                done(k);
        }

        public void purge() {
            long beg = mBeg;
            long end = beg + mDownloadedBytes - 1;
            for (int i = 0; i < deps.size(); i++) {
                ChunkTask d = deps.get(i);
                if (beg <= d.mBeg && d.mEnd <= end && d.mEnd != d.mExpectedFileSize - 1) // never purge last part
                    finish(d, HttpURLConnection.HTTP_OK);
            }
        }

        public void depends(ChunkTask t) {
            deps.add(t);
            if (ranges == null)
                ranges = new RangeSet(t.mBeg, t.mExpectedFileSize - 1);
            ranges.exclude(t.mBeg, t.mEnd);
            if (done != 0)
                done(t);
        }

        public void finish(ChunkTask t, final int done) {
            deps.remove(t);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    ChunkTask.nativeOnFinish(t.mHttpCallbackID, done, t.mBeg, t.mEnd);
                }
            });
        }

        public void done(ChunkTask t) {
            finish(t, done);
            check();
        }

        public void check() {
            if (ranges.isEmpty())
                map.remove(mUrl);
        }
    }

    public DownloadManager() {
        update = new Runnable() {
            @Override
            public void run() {
                Thread thread = new Thread("Download maps Thread") {
                    @Override
                    public void run() {
                        try {
                            update(false);
                        } catch (Throwable e) {
                            Log.d(TAG, "unable to download maps", e);
                            if(NetworkPolicy.getCurrentNetworkUsageStatus())
                                handler.postDelayed(update, 1 * AlarmManager.MIN1);
                        }
                    }
                };
                thread.start();
            }
        };
        if (!NetworkPolicy.getCurrentNetworkUsageStatus())
            handler.post(init);
        else
            update.run();
    }

    synchronized public void init(Context context) {
        this.context = context;
        if (last == 0)
            init.run(); // not initialized yet, restore version
        else
            update.run(); // index downloaded before init, save version
    }

    synchronized public boolean start(ChunkTask k) {
        return false;
    }

    synchronized public void cancel(ChunkTask t) {
        for (String k : new HashSet<>(map.keySet())) {
            Master v = map.get(k);
            if (v.deps.contains(t)) {
                if (v.done == 0) {
                    v.cancel(false);
                    map.remove(k);
                }
            }
        }
    }

    public String getUrl() {
        return "";
    }

    public String filter(String url) { // legacy
        if (url.isEmpty())
            return url;
        if (url.startsWith("http"))
            return url;
        if (url.endsWith(".ttf") || url.startsWith("World"))
            return LEGACY + url;
        Pattern p = Pattern.compile(REMAP);
        Matcher m = p.matcher(url);
        if (m.matches())
            url = m.group(3);
        return getUrl() + url;
    }

    synchronized public void update(boolean force) throws IOException {
    }

    HashMap<Long, DownloadInfo> progress = new HashMap<>();

    public static class DownloadInfo {
        public String url; // enqueue url
        public String path; // url.getPath()
        public Uri uri; // source uri
        public Uri to; // save file to
        public long id; // DownloadManager id
        public long delayed; // delayed id
        public int status; // DownloadManager status
        public Runnable run; // update progress / complete action

        public DownloadInfo(String u, String filter) {
            this.url = u;
            this.path = Uri.parse(u).getPath();
            this.uri = Uri.parse(filter);
        }
    }

    public long delayed(DownloadInfo info) {
        info.delayed = -100 - progress.size();
        progress.put(info.delayed, info);
        return info.delayed;
    }

    public long download(String url) {
        Log.d(TAG, "enqueue " + url);
        final DownloadInfo info = new DownloadInfo(url, filter(url));
        info.to = Uri.parse(ContentResolver.SCHEME_FILE + Storage.CSS + MapManager.nativeGetFilePathByUrl(info.path));
        if (last == 0)
            return delayed(info);
        return download(info);
    }

    public long download(final DownloadInfo info) {
        Log.d(TAG, "Downloading " + info.uri);
        final android.app.DownloadManager dm = (android.app.DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        android.app.DownloadManager.Request request = new android.app.DownloadManager.Request(info.uri)
                .setNotificationVisibility(android.app.DownloadManager.Request.VISIBILITY_HIDDEN)
                .setDestinationUri(info.to);
        info.id = dm.enqueue(request);
        info.run = new Runnable() {
            @Override
            public void run() {
                android.app.DownloadManager.Query query = new android.app.DownloadManager.Query();
                query.setFilterById(info.id);
                Cursor cursor;
                cursor = dm.query(query);
                try {
                    if (cursor != null && cursor.moveToFirst()) {
                        info.status = cursor.getInt(cursor.getColumnIndex(android.app.DownloadManager.COLUMN_STATUS));
                        long bytesDownloaded = cursor.getLong(cursor.getColumnIndex(android.app.DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                        long bytesTotal = cursor.getLong(cursor.getColumnIndex(android.app.DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
                        // String downloadedFileUri = cursor.getString(cursor.getColumnIndex(android.app.DownloadManager.COLUMN_LOCAL_URI));
                        MapManager.nativeOnDownloadProgress(info.id, bytesDownloaded, bytesTotal);
                        switch (info.status) {
                            case android.app.DownloadManager.STATUS_FAILED:
                            case android.app.DownloadManager.STATUS_SUCCESSFUL:
                                onDownloadFinished(info);
                                break;
                        }
                    }
                } finally {
                    if (cursor != null)
                        cursor.close();
                }
                handler.postDelayed(this, 1000);
            }
        };
        progress.put(info.id, info);
        info.run.run();
        return info.id;
    }

    public void onDownloadFinished(DownloadInfo info) {
        MapManager.nativeOnDownloadFinished(info.status == android.app.DownloadManager.STATUS_SUCCESSFUL, info.id);
        cancel(info.id);
    }

    public void cancel(long id) {
        Log.d(TAG, "Cancel " + id);
        final android.app.DownloadManager dm = (android.app.DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadInfo info = progress.get(id);
        if (info != null) {
            handler.removeCallbacksAndMessages(info.run);
            progress.remove(id);
        }
        dm.remove(id);
    }

    public void cancelDelayed() {
        for (Long id : new ArrayList<>(progress.keySet())) {
            DownloadInfo info = progress.get(id);
            if (id < 0) {
                MapManager.nativeOnDownloadFinished(false, info.delayed);
                cancel(info.delayed);
            }
        }
    }
}
