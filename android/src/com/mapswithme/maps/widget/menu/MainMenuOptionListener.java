package com.mapswithme.maps.widget.menu;

public interface MainMenuOptionListener
{
  void onRecording();
  void onAddPlaceOptionSelected();
  void onSearchGuidesOptionSelected();
  void onDownloadMapsOptionSelected();
  void onSettingsOptionSelected();
  void onShareLocationOptionSelected();
  void onSubwayLayerOptionSelected();
  void onTrafficLayerOptionSelected();
  void onIsolinesLayerOptionSelected();
  void onGuidesLayerOptionSelected();
}
